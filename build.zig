const std = @import("std");
const path = std.fs.path;

const Builder = std.build.Builder;

pub fn build(b: *Builder) !void {
    const build_mode = b.standardReleaseOptions();

    const exe = b.addExecutable("vulkan_samples", "src/main.zig");
    exe.setBuildMode(build_mode);
    
    exe.linkSystemLibrary("vulkan");
    // exe.linkSystemLibrary("VkLayer_khronos_validation");
    exe.addCSourceFile("third_party/volk/volk.c", &[_][]const u8{"-std=c89"});
    exe.install();

    const run_step = exe.run();
    run_step.step.dependOn(b.getInstallStep());

    const run_cmd = b.step("run", "Run the app");
    run_cmd.dependOn(&run_step.step);
}

fn addShader(b: *Builder, exe: var, in_file: []const u8, out_file: []const u8) !void {
    // example:
    // glslc -o shaders/vert.spv shaders/shader.vert
    const dirname = "shaders";
    const full_in = try path.join(b.allocator, &[_][]const u8{ dirname, in_file });
    const full_out = try path.join(b.allocator, &[_][]const u8{ dirname, out_file });

    const run_cmd = b.addSystemCommand(&[_][]const u8{
        "glslc",
        "-o",
        full_out,
        full_in,
    });
    exe.step.dependOn(&run_cmd.step);
}
